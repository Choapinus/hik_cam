import sys
import json
from time import sleep
from constants import *
from main import *

dll = windll.LoadLibrary("HCNetSDK.dll")

dll.NET_DVR_Init()

dll.NET_DVR_SetConnectTime(2000, 1)
dll.NET_DVR_SetReconnect(10000, True)

js = json.load(open('camera_data.json'))

ip = js["ip"].encode('utf-8')
port = js["port"]
user = js["user"].encode('utf-8')
pwd = js["pwd"].encode('utf-8')

login = dll.NET_DVR_Login_V30(ip, port, user, pwd)

if login < 0:
	err = dll.NET_DVR_GetLastError()
	for tp in error_message:
		if err == tp[1]:
			print tp[0]+": "+tp[2]
	dll.NET_DVR_Cleanup()
	exit()


if sys.argv[1] == "record":
	start = dll.NET_DVR_StartDVRRecord(login, 0xffff, int(sys.argv[2])) #login, channel, record_type
        print start
elif sys.argv[1] == "stop":
	stop = dll.NET_DVR_StopDVRRecord(login, 0xffff)
        print stop



dll.NET_DVR_Logout(login)
dll.NET_DVR_Cleanup()


